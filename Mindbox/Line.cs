﻿using System;

namespace Mindbox
{
    public class Line : Figure2D
    {
        private double length;

        // Currently, we don't need to add the Point class
        // to set the coordinates of the line points.
        // But if necessary, we will make this property computed
        // without losing functionality in classes that use Line.
        public double Length 
        {
            get => length;
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Length of the line can't be less than zero.");
                length = value;
            }
        }

        public Line(double length)
        {
            Length = length;
        }
    }
}