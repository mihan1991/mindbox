﻿using System;

namespace Mindbox
{
    public class Triangle : Figure2D, IHasSquare
    {
        #region Public properties

        public Line A { get; private set; }
        public Line B { get; private set; }
        public Line C { get; private set; }

        /// <summary>
        /// Returns True if one of the angles of this triangle is 90 degrees.
        /// </summary>
        public bool IsRight
        {
            get
            {
                // Since we assume that the dimensions of the object cannot be changed,
                // we need to do these calculations only once.
                //
                // The same logic applies to the calculation of Perimeter and Area,
                // but there are no cycles inside their calculations
                // and their recalculation will not critically affect performance.
                if (angles == null)
                {
                    angles = MyGetAngles();
                    foreach (var angle in angles)
                        if (Math.Round(angle, 5) == Math.Round(Math.PI / 2, 5))
                            isRight = true;
                }
                return isRight;
            }
        }

        // By convention, we don't need to give the user access to this field.
        // Since this is a calculated field, we can only make it available within this library.
        internal double Perimeter => A.Length + B.Length + C.Length;

        #region IHasSquare implementation

        /// <summary>
        /// Square of this triangle.
        /// </summary>
        public double Square
        {
            get
            {
                var p = Perimeter / 2;
                return Math.Sqrt(p * (p - A.Length) * (p - B.Length) * (p - C.Length));
            }
        }

        #endregion

        #endregion

        #region Private fields

        private double[] angles;
        private bool isRight;

        #endregion

        #region Constructors

        /// <summary>
        /// Create a new triangle with sides of the specified dimensions.
        /// </summary>
        /// <param name="a">Size of the first side.</param>
        /// <param name="b">Size of the second side.</param>
        /// <param name="c">Size of the third side.</param>
        public Triangle(double a, double b, double c)
            : this(new Line(a), new Line(b), new Line(c))
        {
        }

        /// <summary>
        /// Create a new triangle from specified Lines.
        /// </summary>
        /// <param name="a">The first line.</param>
        /// <param name="b">The second line.</param>
        /// <param name="c">The third line.</param>
        /// <exception cref="Exception">Occurs when an attempt is made to create an impossible triangle.</exception>
        public Triangle(Line a, Line b, Line c)
        {
            A = a;
            B = b;
            C = c;
            if (!MyIsPossible())
                throw new Exception("Such triangle cannot exist.");
        }

        #endregion

        #region Private Methods

        private double[] MyGetAngles()
        {
            var a = A.Length;
            var b = B.Length;
            var c = C.Length;

            return new double[]
            {
                Math.Acos((a * a + b * b - c * c) / (2 * a * b)),
                Math.Acos((a * a + c * c - b * b) / (2 * a * c)),
                Math.Acos((b * b + c * c - a * a) / (2 * b * c))
            };
        }

        private bool MyIsPossible()
        {
            var a = A.Length;
            var b = B.Length;
            var c = C.Length;

            return a < b + c && b < a + c && c < a + b;
        }

        #endregion
    }
}
