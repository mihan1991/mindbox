﻿namespace Mindbox
{
    public interface IHasSquare
    {
        double Square { get; }
    }
}
