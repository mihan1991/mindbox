﻿using System;

namespace Mindbox
{
    // Do we consider a circle of radius 0 to be valid?
    // In this realization such circle assumed as valid.
    public class Circle : Figure2D, IHasSquare
    {
        #region Public properties

        public Line Radius { get; private set; }

        #region IHasSquare implementation

        /// <summary>
        /// Square of this circle.
        /// </summary>
        public double Square => Math.PI * Radius.Length * Radius.Length;

        #endregion

        #endregion region

        #region Constructors

        /// <summary>
        /// Create a new circle with specified radius line.
        /// </summary>
        /// <param name="radius">Line of radius.</param>
        public Circle(Line radius)
        {
            Radius = radius;
        }

        /// <summary>
        /// Create a new circle with specified radius size.
        /// </summary>
        /// <param name="radius">Size of the radius.</param>
        public Circle(double radius) : this(new Line(radius))
        {
        }

        #endregion
    }
}
