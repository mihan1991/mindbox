﻿namespace Mindbox
{
    // It is not specified in the conditions,
    // so we will assume that the dimensions of the object cannot be changed.
    // For objects with other sizes we need to create a new object.
    //
    // We don't implement IHasSquare for all 2D objects.
    // For example, line is a 2D object but has no square.
    public abstract class Figure2D
    {        
        // Here we can add some common properties.
        // For example: color.
    }
}
