﻿using System;

using Mindbox;

namespace LibTesterApp
{
    internal class Program
    {
        // Simple demonstration of the using the Mindbox library.
        // There are created 5 random objects that are inheritors of the Figure2D class,
        // but not everyone has Square property. Then the information about each object displayed on the screen.
        static void Main(string[] args)
        {
            int count = 5;

            Random rnd = new Random();
            Figure2D[] figures = new Figure2D[count];

            for (int i=0; i<count; i++)
            {
                var number = rnd.Next(40);
                if (number >= 0 && number < 10)
                    figures[i] = new Line(rnd.Next(10));
                else if (number >= 10 && number < 20)
                    // An exception may occur here due to the creation of random triangles.
                    try
                    {
                        figures[i] = new Triangle(rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10));
                    }
                    catch { }
                else
                    figures[i] = new Circle(rnd.Next(10));
            }

            foreach(var figure in figures)
            {
                if (figure != null)
                {
                    if (figure is IHasSquare fig)
                        Console.WriteLine($"Figure {figure.GetType().Name} with square = {fig.Square}.");
                    else
                        Console.WriteLine($"Figure {figure.GetType().Name} has not square.");
                }
                else
                    Console.WriteLine("Null");
            }

            Console.Read();
        }
    }
}
