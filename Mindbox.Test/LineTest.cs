using NUnit.Framework;

namespace Mindbox.Test
{
    [TestFixture]
    public class LineTest
    {
        [Test]
        public void LineWithNegativeLengthCreationTest()
        {
            Assert.Catch(() => new Line(-5), "Length of the line can't be less than zero.");
        }

        [TestCase(0, 0)]
        [TestCase(1, 1.0)]
        [TestCase(double.MaxValue, double.MaxValue)]
        public void LineCreationTest(double length, double expectedLength)
        {
            Line line = new Line(length);
            Assert.AreEqual(line.Length, expectedLength, 1e-5);
        }
    }
}