﻿using NUnit.Framework;
using System;

namespace Mindbox.Test
{
    [TestFixture]
    public class CircleTest
    {
        [TestCase(0, 0)]
        [TestCase(1, Math.PI)]
        [TestCase(10, 314.159265)]
        public void CircleGetSquareTest(double r, double expected)
        {
            Circle circle = new Circle(r);
            Assert.AreEqual(circle.Square, expected, 1e-5);
        }

        [TestCase(-1)]
        public void ImpossibleTriangleCreationTest(double r)
        {
            Assert.Catch(() => new Circle(r), "Such circle cannot exist.");
        }
    }
}
