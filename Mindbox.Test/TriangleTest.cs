﻿using NUnit.Framework;

namespace Mindbox.Test
{
    [TestFixture]
    public class TriangleTest
    {
        [TestCase(1, 1, 3)]
        [TestCase(2, 3, 5)]
        public void ImpossibleTriangleCreationTest(double a, double b, double c)
        {
            Assert.Catch(() => new Triangle(a,b,c), "Such triangle cannot exist.");
        }

        [TestCase(3, 4, 5, 6)]
        [TestCase(13, 14, 15, 84)]
        [TestCase(10, 10, 8, 36.6606)]
        [TestCase(2, 2, 2, 1.73205)]
        public void TriangleGetSquareTest(double a, double b, double c, double expected)
        {
            Triangle triangle = new Triangle(a,b,c);
            Assert.AreEqual(triangle.Square, expected, 1e-5);
        }

        [TestCase(2, 2, 2, false)]
        [TestCase(3, 4, 5, true)]
        [TestCase(5, 13, 12, true)]
        public void IsTriangleRightTest(double a, double b, double c, bool expected)
        {
            Triangle triangle = new Triangle(a, b, c);
            Assert.AreEqual(triangle.IsRight, expected);
        }
    }
}
